module tps-fake-dashboard

go 1.14

require (
	github.com/go-redis/redis/v8 v8.0.0-beta.7
	github.com/gofiber/fiber v1.13.3
	github.com/gofiber/websocket v0.5.1
	github.com/orcaman/concurrent-map v0.0.0-20190826125027-8c72a8bb44f6
)
