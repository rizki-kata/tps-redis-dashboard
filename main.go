package main

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber"
	"github.com/gofiber/websocket"
	cmap "github.com/orcaman/concurrent-map"
)

var ctx = context.Background()

const tick = 1000

func main() {
	// rdb := redis.NewClient(&redis.Options{
	// 	Addr:     "localhost:6379",
	// 	Password: "",
	// 	DB:       0,
	// })
	rdb := redis.NewClient(&redis.Options{
		// Addr: "172.16.56.198:6379", // <-- dev
		Addr:     "gke-redis.internalstag.katalabs.io:6379", // <-- staging
		Password: "",
		DB:       0,
	})
	tps = cmap.New()
	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		log.Panicf("Failed to connect redis host: %s", err.Error())
	}

	app := fiber.New()
	app.Use(func(c *fiber.Ctx) {
		if websocket.IsWebSocketUpgrade(c) {
			c.Locals("allowed", true)
		}
		c.Next()
	})

	conclusion := ResponseConclusion{}
	app.Post("/bot-response", func(c *fiber.Ctx) {
		var rp *GenericBotResponse = &GenericBotResponse{}
		c.BodyParser(rp)
		r := *rp
		fmt.Println(r)
		m := r.Messages
		if len(m) == 0 {
			conclusion.InvalidCount++
			c.Send("invalid")
			return
		}
		content := m[0].Content
		if strings.HasPrefix(content, "Pesan kamu terproses") {
			conclusion.SuccessCount++
			c.Send("success")
			return
		}
		conclusion.ErrorCount++
		c.Send("not success")
	})

	app.Get("/reset-counter", func(c *fiber.Ctx) {
		conclusion.InvalidCount = 0
		conclusion.ErrorCount = 0
		conclusion.SuccessCount = 0
		c.Send("Counter reset")
	})

	app.Get("/responses", func(c *fiber.Ctx) {
		c.JSON(conclusion)
	})

	app.Get("/ws/tps", websocket.New(func(c *websocket.Conn) {
		dashboardListener++
		go getListBot(rdb)

		go (func() {
			for {
				messageType, _, err := c.NextReader()
				if err != nil {
					log.Printf("Error next reader: %s", err.Error())
					dashboardListener--
					break
				}
				if messageType == -1 || messageType == websocket.CloseMessage {
					if dashboardListener > 0 {
						dashboardListener--
					}
					break
				}
			}
		})()

		for {
			limits := listBot
			if len(limits) > 0 {
				if err := c.WriteJSON(ToMessage("limits", listBot)); err != nil {
					log.Println("write:", err)
					break
				}
			}
			time.Sleep(tick * time.Millisecond)
		}

	}))

	app.Get("/ws/tps/:id", websocket.New(func(c *websocket.Conn) {
		id := c.Params("id")
		mbot, ok := tps.Get("id")
		var bot Tps
		if !ok {
			tps.Set(id, Tps{Listener: 1})
		} else {
			bot := mbot.(Tps)
			bot.Listener++
			tps.Set(id, bot)
		}
		go getTps(rdb, id)

		go (func() {
			for {
				messageType, _, err := c.NextReader()
				if err != nil {
					log.Printf("Error next reader: %s", err.Error())
					if bot.Listener > 0 {
						bot.Listener--
						tps.Set(id, bot)
					}
					break
				}
				if messageType == -1 || messageType == websocket.CloseMessage {
					if bot.Listener > 0 {
						bot.Listener--
						tps.Set(id, bot)
					}
					break
				}
			}
		})()

		for {
			bot, _ := tps.Get(id)
			if err := c.WriteJSON(ToMessage("tps", bot)); err != nil {
				log.Println("write:", err)
				break
			}
			time.Sleep(tick * time.Millisecond)
		}

	}))

	log.Fatal(app.Listen(3000))
}

var listBot map[string]string

var dashboardListener int
var isDashboardListening bool
var tps cmap.ConcurrentMap

type Message struct {
	Type    string      `json:"type"`
	Payload interface{} `json:"payload"`
}

func ToMessage(t string, v interface{}) Message {
	return Message{
		Type:    t,
		Payload: v,
	}
}

type Tps struct {
	LimitIn     int  `json:"limitIn"`
	LimitOut    int  `json:"limitOut"`
	UsageIn     int  `json:"usageIn"`
	UsageOut    int  `json:"usageOut"`
	Listener    int  `json:"-"`
	IsListening bool `json:"-"`
}

func ToTps(limit map[string]string, usageIn int64, usageOut int64) (tps Tps) {
	tps.LimitIn, _ = strconv.Atoi(limit["limitIn"])
	tps.LimitOut, _ = strconv.Atoi(limit["limitOut"])
	tps.UsageIn = int(usageIn)
	tps.UsageOut = int(usageOut)

	return tps
}

func getListBot(client *redis.Client) {
	if isDashboardListening {
		return
	}
	isDashboardListening = true
	for {
		if dashboardListener > 0 {
			// for free
			res := client.HGetAll(ctx, "tps.limit.free")
			freeTps := ToTps(res.Val(), 0, 0)
			// for paid
			res = client.HGetAll(ctx, "tps.bot")
			listBot = res.Val()
			listBot["free"] = fmt.Sprintf("%d", freeTps.LimitIn)
			time.Sleep(tick * time.Millisecond)
		} else {
			isDashboardListening = false
			break
		}
	}
}

func getTps(client *redis.Client, id string) {
	mbot, _ := tps.Get(id)
	bot := mbot.(Tps)
	if bot.IsListening {
		return
	}
	bot.IsListening = true
	tps.Set(id, bot)
	for {
		if bot.Listener > 0 {
			redisLimitKey := "tps.limit.free"
			redisUsageInKey := "tps.usageIn.free"
			redisUsageOutKey := "tps.usageOut.free"

			if id != "free" {
				redisLimitKey = fmt.Sprintf("tps.limit.paid.%s", id)
				redisUsageInKey = fmt.Sprintf("tps.usageIn.paid.%s", id)
				redisUsageOutKey = fmt.Sprintf("tps.usageOut.paid.%s", id)
			}

			limit := client.HGetAll(ctx, redisLimitKey)
			usageIn := client.HLen(ctx, redisUsageInKey)
			usageOut := client.HLen(ctx, redisUsageOutKey)

			val := ToTps(limit.Val(), usageIn.Val(), usageOut.Val())

			tps.Set(id, val)

			time.Sleep(tick * time.Millisecond)
		} else {
			bot.IsListening = false
			tps.Set(id, bot)
			break
		}
	}
}

type ResponseConclusion struct {
	InvalidCount int `json:"invalid_count"`
	ErrorCount   int `json:"error_count"`
	SuccessCount int `json:"success_count"`
}

type GenericBotResponse struct {
	Messages []GenericBotMessage `json:"messages"`
}

type GenericBotMessage struct {
	Action  string `json:"action"`
	Content string `json:"content"`
	Flow    string `json:"flow"`
	ID      string `json:"id"`
	Intent  string `json:"intent"`
	RefID   string `json:"refId"`
	Type    string `json:"type"`
}
